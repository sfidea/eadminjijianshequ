﻿DROP TABLE IF EXISTS `es_dingdan`;
CREATE TABLE `es_dingdan`(
  `id` varchar(255) NOT NULL  COMMENT '订单ID',
  `trade_no` varchar(255) NOT NULL  COMMENT '支付宝订单ID',
  `jiage` decimal(16,2) NOT NULL,
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '积分',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所有人',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `errorcode` varchar(255) NOT NULL  COMMENT '错误代码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='订单表' ;