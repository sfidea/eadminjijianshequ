<?php
namespace addon\alipay\controller;

use app\common\controller\AddonBase;
use addon\alipay\logic\Alipay as LogicAlipay;


class Alipay extends AddonBase{
	
	private static $alipayLogic = null;
	private static $PayModel=null;
	protected $config;
	protected function _initialize()
	{
		$config = $this->getConfig('');
		$this->config=$config;
	
		self::$PayModel = model('Pay', 'service');
		
		self::$PayModel ->setDriver('Alipay');
		
		
		
		
		
		
		
		
	}
	public function payhtml(){
	
		$config = $this->getConfig('alipay');
    	
    	$this->assign('config',$config);
	
		
		return $this->addonTemplate('/payhtml');
	}
	public function notify_url(){
		$verify_result = self::$PayModel->notify();
		
		
		if($verify_result) {//验证成功
		
			$out_trade_no = $_POST['out_trade_no'];
		
			//支付宝交易号
		
			$trade_no = $_POST['trade_no'];
		
			//交易状态
			$trade_status = $_POST['trade_status'];
		
			$map['id']=$out_trade_no;
			$info=Db::name('dingdan')->where($map)->find();

			
			if($info['status']!=1){
				Db::name('dingdan')->where($map)->update(['trade_no' => $trade_no, 'status' => 1]);
				
				point_note($info['score'], $info['uid'], 'alipay');
			}
		
		   echo "success";
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		else {
			//验证失败
			if($info['errorcode']!=0){
				Db::name('dingdan')->where($map)->update(['trade_no' => $trade_no, 'errorcode' => $_POST['trade_status']]);
			}

			echo "fail";
		
		}
	}
	public function return_url(){
	
		$verify_result = self::$PayModel->notify();
		if($verify_result) {//验证成功
		
			//商户订单号
		
			$out_trade_no = $_GET['out_trade_no'];
		
			//支付宝交易号
		
			$trade_no = $_GET['trade_no'];
		
			//交易状态
			$trade_status = $_GET['trade_status'];
		
			$map['id']=$out_trade_no;
			$info=Db::name('dingdan')->where($map)->find();

				if($info['status']!=1){
					Db::name('dingdan')->where($map)->update(['trade_no' => $trade_no, 'status' => 1]);
						
					point_note($info['score'], $info['uid'], 'alipay');
				}

			
			$this->success('支付成功','index/index');
		
		}
		else {

			Db::name('dingdan')->where($map)->update(['trade_no' => $trade_no, 'errorcode' => $_POST['trade_status']]);
			
			$this->error('支付失败','index/index');

		}
		
		
		
		
	}
	public function paysubmit(){
	
		
		
		$paydata=$this->request->param();
		
		
		
		//付款金额，必填
		$total_fee = $paydata['jiage'];
		
		if($total_fee< $this->config['minnum']){
			$this->error('充值金额低于最小金额');
		}
		
		
	
		
		
		
		$out_trade_no = generate_password(16).time();
		
		//订单名称，必填
		$subject = '积分充值';
		
		
		
		//商品描述，可空
		$body = '积分充值';
		
		$data['uid']=session('userid');
		$data['id']=$out_trade_no;
		$data['trade_no']=0;
		$data['status']=0;
		$data['jiage']=$total_fee;
		$data['add_time']=time();
		$data['errorcode']=0;
		$data['score']=$total_fee*$this->config['scorenum'];
		Db::name('dingdan')->insert($data);
		
	
	
		
		$parameter = array(
				"order_sn"	=> $out_trade_no,
				"subject"	=> $subject,
				"order_amount"	=> $total_fee,
				"body"	=> $body,		
		);
		self::$PayModel->pay($parameter);
		
		
	
	}
}
