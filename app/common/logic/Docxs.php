<?php

namespace app\common\logic;

/**
 * 悬赏逻辑
 */
class Docxs extends LogicBase
{
    
    // 会员模型
    public static $docxsModel = null;
    
    /**
     * 构造方法
     */
    public function __construct()
    {
        
        parent::__construct();
        
        self::$docxsModel = model($this->name);
    }
    
    /**
     * 获取悬赏信息
     */
    public function getDocxsInfo($where = [], $field = true)
    {
        
        return self::$docxsModel->getInfo($where, $field);
    }
    
    /**
     * 获取悬赏列表
     */
    public function getDocxsList($where = [], $field = true, $order = '')
    {
    	return self::$docxsModel->getList($where, 'm.*,user.username,doccate.name as tidname,groupcate.name as gidname', $order,0,[['user','m.uid=user.id'],['doccate','m.tid=doccate.id'],['groupcate','m.gid=groupcate.id']]);
    }
    
    /**
     * 获取悬赏列表搜索条件
     */
    public function getWhere($data = [])
    {
        
        $where = [];
        
        !empty($data['search_data']) && $where['name'] = ['like', '%'.$data['search_data'].'%'];
        
        if (!is_administrator()) {
            
         
        }
        
        return $where;
    }
    
  
    /**
     * 悬赏添加
     */
    public function docxsAdd($data = [])
    {
       
    	$validate = validate($this->name);
    	
    	$validate_result = $validate->scene('add')->check($data);
    	
    	if (!$validate_result) : return [RESULT_ERROR, $validate->getError()]; endif;
       
        return self::$docxsModel->setInfo($data) ? [RESULT_SUCCESS, '悬赏添加成功'] : [RESULT_ERROR, self::$docxsModel->getError()];
    }
    /**
     * 悬赏编辑
     */
    public function docxsEdit($data = [],$info)
    {
    	$validate = validate($this->name);
    	
    	$validate_result = $validate->scene('edit')->check($data);
    	
    	if (!$validate_result) : return [RESULT_ERROR, $validate->getError()]; endif;
    	 
       return self::$docxsModel->setInfo($data) ? [RESULT_SUCCESS, '悬赏编辑成功'] : [RESULT_ERROR, '1'];
    }
    /**
     * 设置悬赏信息
     */
    public function setDocxsValue($where = [], $field = '', $value = '')
    {
       
        return self::$docxsModel->setFieldValue($where, $field, $value) ? [RESULT_SUCCESS, '状态更新成功'] : [RESULT_ERROR, self::$docxsModel->getError()];
    }
    /**
     * 更新悬赏信息
     */
    public function setDocxsInfo($data)
    {
    
    	return self::$docxsModel->setInfo($data) ? [RESULT_SUCCESS, '悬赏更新成功'] : [RESULT_ERROR, self::$docxsModel->getError()];
    }
    
    /**
     * 悬赏批量审核
     */
    public function setDocxsAllSh($ids)
    {
    	 
    
    	return self::$docxsModel->setFieldValue(['id'=>array('in',$ids)],'status',1) ? [RESULT_SUCCESS, '悬赏批量审核成功'] : [RESULT_ERROR, self::$docxsModel->getError()];
    }
    /**
     * 悬赏批量删除
     */
    public function docxsAlldel($ids)
    {
    	

    return self::$docxsModel->deleteAllInfo(['id'=>array('in',$ids)]) ? [RESULT_SUCCESS, '悬赏删除成功'] : [RESULT_ERROR, self::$docxsModel->getError()];
    }  
    /**
     * 悬赏删除
     */
    public function docxsDel($where = [])
    {
        
      
        
        return self::$docxsModel->deleteInfo($where) ? [RESULT_SUCCESS, '悬赏删除成功'] : [RESULT_ERROR, self::$docxsModel->getError()];
    }
}
