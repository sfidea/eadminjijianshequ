<?php
namespace app\index\controller;
use app\common\controller\HomeBase;

use app\common\logic\Common as LogicCommon;

class Index extends  HomeBase
{

	private static $commonLogic = null;
	
	public function _initialize()
	{
		parent::_initialize();
		self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);
	
	}
	
   public function index(){
   
   
   	$uid=is_login();
   	
   //热门小组列表
   $hotgrouplist=self::$commonLogic->getDataList('group',['status'=>1],true,'sort desc,membercount desc,topiccount desc',false,'','',6);
   $this->assign('hotgrouplist',$hotgrouplist);
   	
   	if($uid>0){
   		
   	
   		
   		
   	}

   	$hottopiclist=self::$commonLogic->getDataList('topic',['m.status'=>1],'m.*,user.nickname,user.userhead,group.name,group.cover_id','m.update_time desc,m.view desc,m.choice desc',false,[['user','user.id=m.uid','LEFT'],['group','group.id=m.tid']],'',6);
   	foreach ($hottopiclist as $k =>$v){
   		 
   		$comment=model('comment')->where(['fid'=>$v['id']])->order('create_time desc')->limit(1)->select();
   		if($comment){
   			$hottopiclist[$k]['ccreate_time']=$comment[0]['create_time'];
   			$hottopiclist[$k]['cuid']=$comment[0]['uid'];
   		}
   		 
   	}
   	
   	
   	
   	$this->assign('hottopiclist',$hottopiclist);
   	
   	
   	//为你推荐4
   	$tjgrouplist=self::$commonLogic->getDataList('group',['status'=>1,'choice'=>1],true,'update_time desc',false,'','',4);
   	$this->assign('tjgrouplist',$tjgrouplist);
   	//最新话题5
   	$newtopiclist=self::$commonLogic->getDataList('topic',['status'=>1],true,'create_time desc',false,'','',5);
   	$this->assign('newtopiclist',$newtopiclist);
   	
   	
   	return $this->fetch();
   	
   }

}
